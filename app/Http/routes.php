<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::auth();

Route::get('/', 'HomeController@welcome');
Route::get('/home', 'HomeController@index');
Route::get('/messages/{id}', ['as'=>'messages','uses'=>'HomeController@messages']);
Route::get('/notify/{id}/{redirect}', ['as'=>'notify','uses'=>'HomeController@notify']);
Route::post('/postMessage',['as'=>'postMessage','uses'=> 'HomeController@postMessage']);
