<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Models\Message;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usersList = User::all();
        return view('home',compact('usersList'));
    }

    public function notify($id,$redirect)
    {
        $message = Message::findOrFail($id);
        $message->read = 1;
        $message->save();
        return redirect()->route('messages', ['id' => $redirect]);
    }

    public function messages($r_id)
    {
        $messages = Message::all();
        return view('messages',compact('r_id','messages'));
    }

    public function welcome()
    {
        $messages = Message::where('reciever_id','=',Auth::User()->id)->where('read','=',0)->get();
        return view('welcome',compact('r_id','messages'));
    }

    public function postMessage(Request $request)
    {
        $res = $request->all();
        Message::create($res);
        return back();
    }



}
