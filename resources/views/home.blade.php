@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">List of All Users With Role</div>

                <div class="panel-body">
                    @foreach($usersList as $user)
                        <a href="{{URL::to('messages',[$user->id])}}">{{$user->name}} | {{$user->role}}</a>
                        <br>

                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
