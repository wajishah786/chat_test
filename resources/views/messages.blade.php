@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Messages</div>


                    <!-- displays errors -->
                    @if($errors->any())
                        <ul>
                            @foreach($errors->all() as $error)
                                <li style="color: #EA503D;display: block;">{{$error}}</li>
                            @endforeach
                        </ul>
                    @endif

                    @if(Auth::User()->id == $r_id)
                        <p class="alert-danger">Cant send message to yourself</p>
                    @else
                        <div class="panel-body">
                            @foreach($messages as $message)
                                <div style="background-color: #f7f7f7;text-align: center">
                                    @if(Auth::User()->id == $message->sender_id)
                                        <div style="margin-left: 0px;max-width: 200px">
                                            @else
                                                <div style="margin-left: 150px;max-width: 200px">
                                                    @endif
                                                    <p>{{$message->message}}</p>
                                                    <h6 style="background-color: #999999;">
                                                        Time: {{$message->created_at}}
                                                    </h6>
                                                </div>

                                                @endforeach
                                        </div>
                                </div>
                                <div >
                                    <form method="post" action="{{route('postMessage')}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="sender_id" value="{{Auth::User()->id}}">
                                        <input type="hidden" name="reciever_id" value="{{$r_id}}">
                                        <input type="hidden" name="read" value=0>
                                        <textarea name="message" cols="110" rows="10" class="txt fill-width" type="text" placeholder="Your Message here"></textarea>

                                        <div class="clear"></div>
                                        <input type="submit" value="Send">
                                    </form>

                                </div>
                                @endif

                        </div>
                </div>
            </div>
        </div>
@endsection